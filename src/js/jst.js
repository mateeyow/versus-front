this.JST = {"home": function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="container"><h3>Welcome to Versus Front-End Application</h3><p>To test out API pree the link above</p></div>';

}
return __p
},
"index": function(obj) {
obj || (obj = {});
var __t, __p = '', __e = _.escape;
with (obj) {
__p += '<div class="container"><h2>{{vm.main.name}}</h2><ul ng-repeat="(key, value) in vm.main.properties"><li ng-if="!value.hasOwnProperty(\'properties\') &amp;&amp; value.value === true">{{key.split(\'_\').join(\' \')}}</li><li ng-if="!value.hasOwnProperty(\'properties\') &amp;&amp; value.value !== true">{{key.split(\'_\').join(\' \')}}: {{value.value}} {{value.unit}}</li><li ng-if="value.hasOwnProperty(\'properties\')"> <strong>{{key.split(\'_\').join(\' \')}}</strong><ul ng-repeat="(k, v) in value.properties"><li ng-if="!v.hasOwnProperty(\'properties\') &amp;&amp; v.value === true">{{k.split(\'_\').join(\' \')}}</li><li ng-if="!v.hasOwnProperty(\'properties\') &amp;&amp; v.value !== true">{{k.split(\'_\').join(\' \')}}: {{v.value}} {{v.unit}}</li><li ng-if="v.hasOwnProperty(\'properties\')"><i>{{k.split(\'_\').join(\' \')}}</i><ul ng-repeat="(ke, val) in v.properties"><li ng-if="!val.hasOwnProperty(\'properties\') &amp;&amp; val.value === true">{{ke.split(\'_\').join(\' \')}}</li><li ng-if="!val.hasOwnProperty(\'properties\') &amp;&amp; val.value !== true">{{ke.split(\'_\').join(\' \')}}: {{val.value}} {{val.unit}}</li></ul></li></ul></li></ul></div>';

}
return __p
}};