'use strict'
var versusConfig = function ($stateProvider, $locationProvider, $urlRouterProvider) {
  $locationProvider.html5Mode({enabled: true, requireBase: false});

  $stateProvider
  .state('index', {
    url: '/',
    template: JST['home']()
  })
  .state('phone', {
    url: '/:url',
    template: JST['index'](),
    controller: IndexCtrl,
    controllerAs: 'vm',
    resolve: {
      Data: function (dataService, $stateParams, $q) {
        // return dataService($stateParams.url);
        var defer = $q.defer();

        dataService($stateParams.url)
        .then(function (response) {
          defer.resolve(response.data)
        })
        .catch(function (err) {
          defer.reject(err.data);
        });

        return defer.promise;
      }
    }
  });

};

var versusRun = function ($rootScope, $state) {
  $rootScope.$on('$stateChangeStart', function () {
  });

  $rootScope.$on('$stateChangeSuccess', function () {
    $rootScope.isError = false;
  });

  $rootScope.$on('$stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
    $rootScope.isError = true;
  });
};

angular.module('versus', [
  'ui.router',
  'ngAria',
  'ngSanitize'
])
.config(versusConfig)
.run(versusRun);

versusConfig.$inject = [
  '$stateProvider',
  '$locationProvider',
  '$urlRouterProvider'
];