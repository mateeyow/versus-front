'use strict'
var IndexCtrl = function (Data) {
  var vm = this;

  vm.main = Data.base;
  Data.embedded.forEach(function (val) {
    if (val.hasOwnProperty('properties')) {
      var keys = Object.keys(val.properties);

      for (var key in val.properties) {
        var value = val.properties[key];
        var result = _.find(Data.embedded, function (d) {
          return key === d.name
        });
        val.properties[key] = result;
        val.properties[key].value = value;
      }
    }
  });

  for (var key in vm.main.properties) {
    var value = vm.main.properties[key];
    var res = _.find(Data.embedded, function (e) {
      return key === e.name
    });
    vm.main.properties[key] = res;
    vm.main.properties[key].value = value;
  }
};

angular.module('versus')
.controller('IndexCtrl', IndexCtrl)

IndexCtrl.$inject = ['Data']