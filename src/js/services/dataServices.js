'use strict'
var dataService = function ($q, $http) {
  return function (link) {
    // var defer = $q.defer();
    var url = 'http://localhost:3000/' + link;

    return $http.get(url);
    // .then(function (response) {
    //   defer.resolve(response.data)
    // })
    // .catch(function (err) {
    //   defer.reject(err.data);
    // });

    // return defer.promise;
  }
};

angular.module('versus')
.service('dataService', dataService);

dataService.$inject = ['$q', '$http'];