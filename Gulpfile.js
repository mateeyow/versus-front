'use strict'
let es = require('event-stream');
let del = require('del');
let gulp = require('gulp');
let jst = require('gulp-jst-concat');
let jade = require('gulp-jade');
let inject = require('gulp-inject');
let browserSync = require('browser-sync').create();
let sequence = require('run-sequence');
let historyApiFallback = require('connect-history-api-fallback');

let source = gulp.src([
    './vendors/bootstrap/dist/css/bootstrap.min.css',
    './vendors/lodash/lodash.min.js',
    './vendors/angular/angular.js',
    './vendors/angular-aria/angular-aria.js',
    './vendors/angular-sanitize/angular-sanitize.js',
    './vendors/angular-ui-router/release/angular-ui-router.js'
  ], {read: false});

gulp.task('build', function (cb) {
  sequence('clean', 'template', 'inject', 'jade', cb);
});

gulp.task('inject', function () {
  return gulp.src('./src/index.jade')
    .pipe(inject(source, {relative: true, name: 'vendors'}))
    .pipe(inject(gulp.src(['./src/js/**/*.js', './src/css/**/*.css'], {read: false}), {relative: false}))
    .pipe(gulp.dest('./src'));
});

gulp.task('template', function () {
  return gulp.src('./src/templates/**/*.jade')
    .pipe(jade())
    .pipe(jst('jst.js', {
      renameKeys: ['^.*templates/(.*).html$', '$1']
    }))
    .pipe(gulp.dest('./src/js'));
});

gulp.task('jade', function () {
  return gulp.src('./src/index.jade')
    .pipe(jade({
      pretty: true
    }))
    .pipe(gulp.dest('./dist/'));
});

gulp.task('serve', function () {
  browserSync.init({
    server: { 
      baseDir: "./",
      index: './dist/index.html',
      middleware: [historyApiFallback({index: './dist/index.html'})]
    }
  });
  gulp.watch('./src/**/*', ['build']);
  gulp.watch('./dist/index.html').on('change', browserSync.reload);
});

gulp.task('clean', function (cb) {
  return del(['dist/**/*'], cb);
});

gulp.task('default', ['serve']);